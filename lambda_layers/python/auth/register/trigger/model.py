from aws_lambda_powertools.utilities.parser import BaseModel, Field



class CognitoUserModel(BaseModel):
    username: str
    sub: str
    email: str
    PK: str = None
    SK: str = None


