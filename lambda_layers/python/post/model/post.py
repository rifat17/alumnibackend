# import enum

from aws_lambda_powertools.utilities.parser import BaseModel
from typing import List, Optional

import constants


# class PostStatus(enum.Enum):
#     INREVIEW = 'INREVIEW'
#     APPROVED = 'APPROVED'
#     REJECTED = 'REJECTED'
#     DELETED = 'DELETED'



class PostReqBody(BaseModel):
    PK: str
    SK: str
    type: str
    ID: str
    cat_id: str
    cat_name: str
    post_content: str
    file_ids: Optional[List[str]]
    postOwner: str
    tag_persons: Optional[List[str]]
    created_at: str
    sub: str
    post_status: str = constants.INREVIEW

    # approved by? deleted by?



class EditPostBody(BaseModel):
    PK: str
    SK: str
    post_content: str # right now only this is editable




def test(data):
    from pprint import pprint as pp

    pp(data)

    post = PostReqBody(**data)
    pp(post.dict())


def main():
    demo_post = {
        "PK": "POST#I2FqpJTff4cpSphcHikVW",
        "SK": "POST#I2FqpJTff4cpSphcHikVW",
        "type": "POST",
        "ID": "I2FqpJTff4cpSphcHikVW",
        "cat_id": "cat-1",
        "cat_name": "Category-1",
        "post_content": "AAACC",
        "file_ids": [
            "7K-dCWY-YWxcA2cBuOvbP",
            "HptvPJ6_WxaTgTRG4AApS",
            "btENU84Vu-W_Ycxslp65y"
        ],
        "postOwner": "hasib2",
        "tag_persons": [
            "quiet",
            "rose"
        ],
        "created_at": "2021-07-29T18:04:42",
        "sub": "109b592f-9da8-470c-a06d-141864bff36f"
    }

    test(demo_post)


if __name__ == '__main__':
    main()
