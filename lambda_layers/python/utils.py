from pprint import pprint


def log(title, msg, type=None):
    print('*' * 30)
    print(f'{" " * 10} {title} {" " * 10}')
    print('_' * 30)
    print()
    if type is None:
        print(msg)
    elif type == 'dict':
        pprint(msg)
    print()
    print('_' * 30)
    print(f'{" " * 10} LOG END {" " * 10}')
    print('*' * 30)


if __name__ == '__main__':
    log('test', 'mylog')
    log('mylog', {1: '2', 2: {12: "1212"}}, 'dict')
