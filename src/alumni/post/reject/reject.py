import os
import boto3
import json
from aws_lambda_powertools.utilities.parser import BaseModel, ValidationError
from botocore.exceptions import ClientError
import constants




class PostRejectBody(BaseModel):
    PK: str
    SK: str
    post_status: str = constants.REJECTED



def lambda_handler(event, context):
    headers = {
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    print(event)


    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    table: boto3.resource = boto3.resource('dynamodb', region_name=region).Table(table_name)



    try:

        bodydict = json.loads(event['body'])
        post = PostRejectBody(**bodydict)
        print(post)
    except ValidationError as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        response = table.update_item(
            Key={
                'PK': post.PK,
                'SK': post.SK
            },
            UpdateExpression="set  post_status = :ps",
            ExpressionAttributeValues={
                ':ps': post.post_status,
            },
        )
        print(response)

    except ClientError as e:
        print(e)
        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item Rejected')
        return {
            'statusCode': 200,
            'body': post.json(), 
            'headers': headers
        }