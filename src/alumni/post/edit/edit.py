import os
import boto3
import json
from aws_lambda_powertools.utilities.parser import BaseModel, ValidationError
from botocore.exceptions import ClientError


class EditPostBody(BaseModel):
    PK: str
    SK: str
    post_content: str # right now only this is editable




def lambda_handler(event, context):
    headers = {
        'Content-Type' : 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    print(event)

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    table: boto3.resource = boto3.resource('dynamodb', region_name=region).Table(table_name)



    try:

        bodydict = json.loads(event['body'])
        post = EditPostBody(**bodydict)
        print(post)
    except ValidationError as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        response = table.update_item(
            Key={
                'PK': post.PK,
                'SK': post.SK
            },
            UpdateExpression="set  post_content = :pc",
            ExpressionAttributeValues={
                ':pc': post.post_content,
            },
            ReturnValues="UPDATED_NEW"
        )
        print(response)

    except ClientError as e:
        print(e)
        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item Edited')
        return {
            'statusCode': 200,
            'body': post.json(), 
            'headers': headers
        } 

