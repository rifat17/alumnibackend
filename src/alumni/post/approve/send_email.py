from typing import List
import boto3
def send_email(to : List, msg: str):
    ses_client = boto3.client("ses")

    CHARSET = "UTF-8"

    # TEST PERMISSION
    # response = ses_client.list_identities(
    #     IdentityType = 'EmailAddress',
    #     MaxItems=10
    # )
    # print(response)

    response = ses_client.send_email(
        Destination = {
            "ToAddresses" : to,
        },
        Message = {
            "Body" : {
                "Html" : {
                    "Charset": CHARSET,
                    "Data": msg,
                }
            },
            "Subject": {
                "Charset" : CHARSET,
                "Data" : "Alumni Mention Example",
            },
        },
        Source="hasib@baiust.edu.bd"
    )

    print(response)