import os
from typing import List, Optional
import boto3
import json
from aws_lambda_powertools.utilities.parser import BaseModel, ValidationError
from botocore.exceptions import ClientError
from send_email import send_email
import constants

# from lambda_layers.python import constants


class PostApproveBody(BaseModel):
    PK: str
    SK: str
    postOwner: str
    post_status: str = constants.APPROVED
    emails: Optional[List]
    tag_persons: Optional[List]


def generate_msgs(post: PostApproveBody):
    print(post)

    msgs = []
    ID = post.PK.split('#')[1]
    url = "http://localhost:3000"
    uri = f'{url}/post/{ID}'

    # HTML_EMAIL_CONTENT = """
    #     <html>
    #         <head></head>
    #         <h1'>Hi {person},</h1>
    #         <p>{postOwner} mention you in a <a href="{uri}">Post</a></p>
    #         </body>
    #     </html>
    # """
    # print(HTML_EMAIL_CONTENT)

    for person in post.tag_persons:
        msg = """
        <html>
            <head></head>
            <h1'>Hi {nameWhomMentioned},</h1>
            <p>{nameWhoMentioned} mention you in a <a href="{mentionTo}">Post</a></p>
            </body>
        </html>
            """.format(nameWhomMentioned=person, nameWhoMentioned=post.postOwner, mentionTo=uri)
        print(msg)
        msgs.append(msg)
    return msgs


def lambda_handler(event, context):
    headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    print(event)

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    table: boto3.resource = boto3.resource(
        'dynamodb', region_name=region).Table(table_name)

    try:

        bodydict = json.loads(event['body'])
        post = PostApproveBody(**bodydict)
        print(post)
    except ValidationError as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        # if not an admin, next line will raise KeyError for not having key in dict
        # and request will be rejected
        # so far, this is the idea
        isAdmin = event['requestContext']['authorizer']['claims']['cognito:groups']

        response = table.update_item(
            Key={
                'PK': post.PK,
                'SK': post.SK
            },
            UpdateExpression="set  post_status = :ps",
            ExpressionAttributeValues={
                ':ps': post.post_status,
            },
            ReturnValues="UPDATED_NEW"
        )
        print('ApproveResponse', response)
        email_msgs = generate_msgs(post)
        for msg in email_msgs:
            print(msg)
            send_email(to=["hasib@baiust.edu.bd"], msg=msg)

    except (ClientError, KeyError) as e:
        print(e)
        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers, }

    else:
        print('Item Approved')
        return {
            'statusCode': 200,
            'body': post.json(),
            'headers': headers
        }


req_body_with_cognito_info_for_admin = {
    'resource': '/post',
    'path': '/post',
    'httpMethod': 'POST',
    'headers': {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'access-control-allow-origin': '*',
        'Authorization': 'eyJraWQiOiJJc1lFWUV6b3NZNXh1XC8zWTNBUDJQbUJnQWM3cTdHRlNJM2Z0WW05dk5rYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIwYzgwN2M5Yy0zOTU0LTQyZGYtOTM5NC02YzFkNjRiMGNhMTYiLCJjb2duaXRvOmdyb3VwcyI6WyJBZG1pbiJdLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9BQXdsVWpOeUUiLCJjb2duaXRvOnVzZXJuYW1lIjoiaGFzaWIiLCJvcmlnaW5fanRpIjoiZTNiZWQ0NTAtZDY1OS00NTk1LWEzYTMtOGJlNzFlYzFmYWNhIiwiYXVkIjoiM2JpcnZkaTZjNHRxbzRkYjBhNW1wbjlwMzciLCJldmVudF9pZCI6ImNkYjI3OWZhLTJmMDctNDZiNi1hMDI1LTlkMjY5NTIyOWVmMSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjI3Nzg0MTE3LCJleHAiOjE2Mjc3ODc3MTcsImlhdCI6MTYyNzc4NDExNywianRpIjoiZGYyMjQ2MTAtNWYwYy00ZTQwLTg1MGItM2M1ZTU2NWI2NTNiIiwiZW1haWwiOiJoYXNpYkBiYWl1c3QuZWR1LmJkIn0.ln4BJOG6l_rIJ0wcZ9IwPbQdPoTUYy14--IetfntQ3_j5480ceUURBMZWU08rBSh4VXa-Gs6hwu1Qo6abRa3V3QoZzR9b4G3l79fsM9LTwU1drY8b9nF7Rtx74VZ-sQbfjSecelVkFM40YaYY4TklJs6xwnNvBDxjpLqHEz5hxczgJKgb2LsbvX2kQbI8950MrTA6HA5X8b2cca8581G_gb9S3NNP3WvQkHn9AEyGxJFF0aqWEw9LcgzxahfEysgmB9L9arBy8QBlOiQzDBTHHYn1CqRfczL4xPsh2aa9VVI6kc5T2em2SK79Jp4f7Yppu9wG2m0UhnVXzh2mdd6kQ',
        'CloudFront-Forwarded-Proto': 'https',
        'CloudFront-Is-Desktop-Viewer': 'false',
        'CloudFront-Is-Mobile-Viewer': 'true',
        'CloudFront-Is-SmartTV-Viewer': 'false',
        'CloudFront-Is-Tablet-Viewer': 'false',
        'CloudFront-Viewer-Country': 'BD',
        'content-type': 'application/json',
        'Host': 'seacnwhfc8.execute-api.us-east-2.amazonaws.com',
        'origin': 'http://localhost:3000',
        'Referer': 'http://localhost:3000/',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="92"',
        'sec-ch-ua-mobile': '?1',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'cross-site',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
        'Via': '2.0 700b42bdce7a61786b48963819fc092b.cloudfront.net (CloudFront)',
        'X-Amz-Cf-Id': 'FQrD6eCV5uu7QopxXrPRP8e90dfTXLR3xfSpuDoPTDy6RWC5r92sJw==',
        'X-Amzn-Trace-Id': 'Root=1-610603c9-4ad905297987b86a7af9d157',
        'X-Forwarded-For': '27.147.206.128, 130.176.165.136',
        'X-Forwarded-Port': '443',
        'X-Forwarded-Proto': 'https',
    },
    'multiValueHeaders': {
        'Accept': ['application/json, text/plain, */*'],
        'Accept-Encoding': ['gzip, deflate, br'],
        'Accept-Language': ['en-US,en;q=0.9'],
        'access-control-allow-origin': ['*'],
        'Authorization': ['eyJraWQiOiJJc1lFWUV6b3NZNXh1XC8zWTNBUDJQbUJnQWM3cTdHRlNJM2Z0WW05dk5rYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIwYzgwN2M5Yy0zOTU0LTQyZGYtOTM5NC02YzFkNjRiMGNhMTYiLCJjb2duaXRvOmdyb3VwcyI6WyJBZG1pbiJdLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9BQXdsVWpOeUUiLCJjb2duaXRvOnVzZXJuYW1lIjoiaGFzaWIiLCJvcmlnaW5fanRpIjoiZTNiZWQ0NTAtZDY1OS00NTk1LWEzYTMtOGJlNzFlYzFmYWNhIiwiYXVkIjoiM2JpcnZkaTZjNHRxbzRkYjBhNW1wbjlwMzciLCJldmVudF9pZCI6ImNkYjI3OWZhLTJmMDctNDZiNi1hMDI1LTlkMjY5NTIyOWVmMSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjI3Nzg0MTE3LCJleHAiOjE2Mjc3ODc3MTcsImlhdCI6MTYyNzc4NDExNywianRpIjoiZGYyMjQ2MTAtNWYwYy00ZTQwLTg1MGItM2M1ZTU2NWI2NTNiIiwiZW1haWwiOiJoYXNpYkBiYWl1c3QuZWR1LmJkIn0.ln4BJOG6l_rIJ0wcZ9IwPbQdPoTUYy14--IetfntQ3_j5480ceUURBMZWU08rBSh4VXa-Gs6hwu1Qo6abRa3V3QoZzR9b4G3l79fsM9LTwU1drY8b9nF7Rtx74VZ-sQbfjSecelVkFM40YaYY4TklJs6xwnNvBDxjpLqHEz5hxczgJKgb2LsbvX2kQbI8950MrTA6HA5X8b2cca8581G_gb9S3NNP3WvQkHn9AEyGxJFF0aqWEw9LcgzxahfEysgmB9L9arBy8QBlOiQzDBTHHYn1CqRfczL4xPsh2aa9VVI6kc5T2em2SK79Jp4f7Yppu9wG2m0UhnVXzh2mdd6kQ'
                          ],
        'CloudFront-Forwarded-Proto': ['https'],
        'CloudFront-Is-Desktop-Viewer': ['false'],
        'CloudFront-Is-Mobile-Viewer': ['true'],
        'CloudFront-Is-SmartTV-Viewer': ['false'],
        'CloudFront-Is-Tablet-Viewer': ['false'],
        'CloudFront-Viewer-Country': ['BD'],
        'content-type': ['application/json'],
        'Host': ['seacnwhfc8.execute-api.us-east-2.amazonaws.com'],
        'origin': ['http://localhost:3000'],
        'Referer': ['http://localhost:3000/'],
        'sec-ch-ua': ['" Not A;Brand";v="99", "Chromium";v="92"'],
        'sec-ch-ua-mobile': ['?1'],
        'sec-fetch-dest': ['empty'],
        'sec-fetch-mode': ['cors'],
        'sec-fetch-site': ['cross-site'],
        'User-Agent': ['Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36'
                       ],
        'Via': ['2.0 700b42bdce7a61786b48963819fc092b.cloudfront.net (CloudFront)'
                ],
        'X-Amz-Cf-Id': ['FQrD6eCV5uu7QopxXrPRP8e90dfTXLR3xfSpuDoPTDy6RWC5r92sJw=='
                        ],
        'X-Amzn-Trace-Id': ['Root=1-610603c9-4ad905297987b86a7af9d157'
                            ],
        'X-Forwarded-For': ['27.147.206.128, 130.176.165.136'],
        'X-Forwarded-Port': ['443'],
        'X-Forwarded-Proto': ['https'],
    },
    'queryStringParameters': None,
    'multiValueQueryStringParameters': None,
    'pathParameters': None,
    'stageVariables': None,
    # requestContext-> authorizer -> claims -> cognito:groups
    'requestContext': {
        'resourceId': '6b1v5u',
        'authorizer': {'claims': {
            'sub': '0c807c9c-3954-42df-9394-6c1d64b0ca16',
            'cognito:groups': 'Admin',
            'iss': 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_AAwlUjNyE',
            'cognito:username': 'hasib',
            'origin_jti': 'e3bed450-d659-4595-a3a3-8be71ec1faca',
            'aud': '3birvdi6c4tqo4db0a5mpn9p37',
            'event_id': 'cdb279fa-2f07-46b6-a025-9d2695229ef1',
            'token_use': 'id',
            'auth_time': '1627784117',
            'exp': 'Sun Aug 01 03:15:17 UTC 2021',
            'iat': 'Sun Aug 01 02:15:17 UTC 2021',
            'jti': 'df224610-5f0c-4e40-850b-3c5e565b653b',
            'email': 'hasib@baiust.edu.bd',
        }},
        'resourcePath': '/post',
        'httpMethod': 'POST',
        'extendedRequestId': 'DXWHgHExiYcFhDw=',
        'requestTime': '01/Aug/2021:02:15:37 +0000',
        'path': '/dev/post',
        'accountId': '618758721119',
        'protocol': 'HTTP/1.1',
        'stage': 'dev',
        'domainPrefix': 'seacnwhfc8',
        'requestTimeEpoch': 1627784137491,
        'requestId': 'd2197420-7660-47b9-8a8e-33d399b8f924',
        'identity': {
            'cognitoIdentityPoolId': None,
            'accountId': None,
            'cognitoIdentityId': None,
            'caller': None,
            'sourceIp': '27.147.206.128',
            'principalOrgId': None,
            'accessKey': None,
            'cognitoAuthenticationType': None,
            'cognitoAuthenticationProvider': None,
            'userArn': None,
            'userAgent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
            'user': None,
        },
        'domainName': 'seacnwhfc8.execute-api.us-east-2.amazonaws.com',
        'apiId': 'seacnwhfc8',
    },
    'body': '{"PK":"POST#t6bJZU-lcdWtzHS2OXlu_","SK":"POST#t6bJZU-lcdWtzHS2OXlu_","type":"POST","ID":"t6bJZU-lcdWtzHS2OXlu_","cat_id":"cat-1","cat_name":"Category-1","post_content":"POSTOOOO","file_ids":["70Jigdo2lC8zlNgTv_evq","rWyBPEEJ99kHq8Yh57DvJ","rx__hU_YSxiTsuuKSlvir"],"postOwner":"hasib","tag_persons":["north"],"created_at":"1627784135520","sub":"0c807c9c-3954-42df-9394-6c1d64b0ca16"}',
    'isBase64Encoded': False,
}

req_body_with_cognito_info_for_user = {
    'resource': '/post',
    'path': '/post',
    'httpMethod': 'POST',
    'headers': {
        'Accept': 'application/json, text/plain, */*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'access-control-allow-origin': '*',
        'Authorization': 'eyJraWQiOiJJc1lFWUV6b3NZNXh1XC8zWTNBUDJQbUJnQWM3cTdHRlNJM2Z0WW05dk5rYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhNjNhZWJkZS0xMzI1LTRiZGItOWZmNi1hMDdhYTVkMmUyM2UiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tXC91cy1lYXN0LTJfQUF3bFVqTnlFIiwiY29nbml0bzp1c2VybmFtZSI6Imhhc2liMiIsIm9yaWdpbl9qdGkiOiI4YmNmZTIyMy0zYmI5LTRjYzItYmJiYi05ZjUwYjNjOWE4NDciLCJhdWQiOiIzYmlydmRpNmM0dHFvNGRiMGE1bXBuOXAzNyIsImV2ZW50X2lkIjoiMmUzMGYxYjMtOTRkYi00NjdmLThmY2EtNDNmZDczZjQ4MzRlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2Mjc3ODM3MjMsImV4cCI6MTYyNzc4NzMyMywiaWF0IjoxNjI3NzgzNzI0LCJqdGkiOiI3YmU3ZTZjYS1hY2Q1LTQ1ZjItYTg3Zi0xNmYxZDQ5YjJmZWUiLCJlbWFpbCI6ImFiZHVsbGFoLnF1cHNAZ21haWwuY29tIn0.MWpMiqRtRKs5qygMluNhKhnrJsMRtmJ-mr6OZpo2fC9Q8jIonF6BvWg-QAF25Qqz_fiA2RuTW2-i9cxTAtP6_So2irJfKWbV6k2YAEm8awRSM_KYc_umTBhkMfq9bzdoTnFrbhD1PUFjJU3nwyIlT-BBo_rg0-OnYWI1ca1UJGgx__o74b_X7XcjmJLqHtxsa20gVzH-n1QdR-i0ivRrG1oWEZXtcMjggtUX6RJW-8a1QFYyQ2k2XYZTWwhiN2wvkeIsaDH_x8Jt9KR64Sa6cSH-O710OeN-X3A_YVW9FdAEC6jNBfEwMgH9wzCrAhCnFCrAYTQ_m2F_KrQPtdkC1Q',
        'CloudFront-Forwarded-Proto': 'https',
        'CloudFront-Is-Desktop-Viewer': 'false',
        'CloudFront-Is-Mobile-Viewer': 'true',
        'CloudFront-Is-SmartTV-Viewer': 'false',
        'CloudFront-Is-Tablet-Viewer': 'false',
        'CloudFront-Viewer-Country': 'BD',
        'content-type': 'application/json',
        'Host': 'seacnwhfc8.execute-api.us-east-2.amazonaws.com',
        'origin': 'http://localhost:3000',
        'Referer': 'http://localhost:3000/',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="92"',
        'sec-ch-ua-mobile': '?1',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'cross-site',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
        'Via': '2.0 ac7f86953c14e20f2f8ac129a44475d0.cloudfront.net (CloudFront)',
        'X-Amz-Cf-Id': '5ehnGMaqqmee_FgKI5Xw-TFsF3dAMqsiHxVkVIcNJX4iTbRbQG7pXg==',
        'X-Amzn-Trace-Id': 'Root=1-61060233-7cbdbcc30747d7764d69c251',
        'X-Forwarded-For': '27.147.206.128, 130.176.165.151',
        'X-Forwarded-Port': '443',
        'X-Forwarded-Proto': 'https',
    },
    'multiValueHeaders': {
        'Accept': ['application/json, text/plain, */*'],
        'Accept-Encoding': ['gzip, deflate, br'],
        'Accept-Language': ['en-US,en;q=0.9'],
        'access-control-allow-origin': ['*'],
        'Authorization': ['eyJraWQiOiJJc1lFWUV6b3NZNXh1XC8zWTNBUDJQbUJnQWM3cTdHRlNJM2Z0WW05dk5rYz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhNjNhZWJkZS0xMzI1LTRiZGItOWZmNi1hMDdhYTVkMmUyM2UiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tXC91cy1lYXN0LTJfQUF3bFVqTnlFIiwiY29nbml0bzp1c2VybmFtZSI6Imhhc2liMiIsIm9yaWdpbl9qdGkiOiI4YmNmZTIyMy0zYmI5LTRjYzItYmJiYi05ZjUwYjNjOWE4NDciLCJhdWQiOiIzYmlydmRpNmM0dHFvNGRiMGE1bXBuOXAzNyIsImV2ZW50X2lkIjoiMmUzMGYxYjMtOTRkYi00NjdmLThmY2EtNDNmZDczZjQ4MzRlIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2Mjc3ODM3MjMsImV4cCI6MTYyNzc4NzMyMywiaWF0IjoxNjI3NzgzNzI0LCJqdGkiOiI3YmU3ZTZjYS1hY2Q1LTQ1ZjItYTg3Zi0xNmYxZDQ5YjJmZWUiLCJlbWFpbCI6ImFiZHVsbGFoLnF1cHNAZ21haWwuY29tIn0.MWpMiqRtRKs5qygMluNhKhnrJsMRtmJ-mr6OZpo2fC9Q8jIonF6BvWg-QAF25Qqz_fiA2RuTW2-i9cxTAtP6_So2irJfKWbV6k2YAEm8awRSM_KYc_umTBhkMfq9bzdoTnFrbhD1PUFjJU3nwyIlT-BBo_rg0-OnYWI1ca1UJGgx__o74b_X7XcjmJLqHtxsa20gVzH-n1QdR-i0ivRrG1oWEZXtcMjggtUX6RJW-8a1QFYyQ2k2XYZTWwhiN2wvkeIsaDH_x8Jt9KR64Sa6cSH-O710OeN-X3A_YVW9FdAEC6jNBfEwMgH9wzCrAhCnFCrAYTQ_m2F_KrQPtdkC1Q'
                          ],
        'CloudFront-Forwarded-Proto': ['https'],
        'CloudFront-Is-Desktop-Viewer': ['false'],
        'CloudFront-Is-Mobile-Viewer': ['true'],
        'CloudFront-Is-SmartTV-Viewer': ['false'],
        'CloudFront-Is-Tablet-Viewer': ['false'],
        'CloudFront-Viewer-Country': ['BD'],
        'content-type': ['application/json'],
        'Host': ['seacnwhfc8.execute-api.us-east-2.amazonaws.com'],
        'origin': ['http://localhost:3000'],
        'Referer': ['http://localhost:3000/'],
        'sec-ch-ua': ['" Not A;Brand";v="99", "Chromium";v="92"'],
        'sec-ch-ua-mobile': ['?1'],
        'sec-fetch-dest': ['empty'],
        'sec-fetch-mode': ['cors'],
        'sec-fetch-site': ['cross-site'],
        'User-Agent': ['Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36'
                       ],
        'Via': ['2.0 ac7f86953c14e20f2f8ac129a44475d0.cloudfront.net (CloudFront)'
                ],
        'X-Amz-Cf-Id': ['5ehnGMaqqmee_FgKI5Xw-TFsF3dAMqsiHxVkVIcNJX4iTbRbQG7pXg=='
                        ],
        'X-Amzn-Trace-Id': ['Root=1-61060233-7cbdbcc30747d7764d69c251'
                            ],
        'X-Forwarded-For': ['27.147.206.128, 130.176.165.151'],
        'X-Forwarded-Port': ['443'],
        'X-Forwarded-Proto': ['https'],
    },
    'queryStringParameters': None,
    'multiValueQueryStringParameters': None,
    'pathParameters': None,
    'stageVariables': None,
    'requestContext': {
        'resourceId': '6b1v5u',
        'authorizer': {'claims': {
            'sub': 'a63aebde-1325-4bdb-9ff6-a07aa5d2e23e',
            'email_verified': 'true',
            'iss': 'https://cognito-idp.us-east-2.amazonaws.com/us-east-2_AAwlUjNyE',
            'cognito:username': 'hasib2',
            'origin_jti': '8bcfe223-3bb9-4cc2-bbbb-9f50b3c9a847',
            'aud': '3birvdi6c4tqo4db0a5mpn9p37',
            'event_id': '2e30f1b3-94db-467f-8fca-43fd73f4834e',
            'token_use': 'id',
            'auth_time': '1627783723',
            'exp': 'Sun Aug 01 03:08:43 UTC 2021',
            'iat': 'Sun Aug 01 02:08:44 UTC 2021',
            'jti': '7be7e6ca-acd5-45f2-a87f-16f1d49b2fee',
            'email': 'abdullah.qups@gmail.com',
        }},
        'resourcePath': '/post',
        'httpMethod': 'POST',
        'extendedRequestId': 'DXVIGEU9CYcF_cA=',
        'requestTime': '01/Aug/2021:02:08:51 +0000',
        'path': '/dev/post',
        'accountId': '618758721119',
        'protocol': 'HTTP/1.1',
        'stage': 'dev',
        'domainPrefix': 'seacnwhfc8',
        'requestTimeEpoch': 1627783731650,
        'requestId': 'b947e574-9467-42ad-94b2-0fed0accfa46',
        'identity': {
            'cognitoIdentityPoolId': None,
            'accountId': None,
            'cognitoIdentityId': None,
            'caller': None,
            'sourceIp': '27.147.206.128',
            'principalOrgId': None,
            'accessKey': None,
            'cognitoAuthenticationType': None,
            'cognitoAuthenticationProvider': None,
            'userArn': None,
            'userAgent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Mobile Safari/537.36',
            'user': None,
        },
        'domainName': 'seacnwhfc8.execute-api.us-east-2.amazonaws.com',
        'apiId': 'seacnwhfc8',
    },
    'body': '{"PK":"POST#gH2NBTAzgPsmqH-3nPkNZ","SK":"POST#gH2NBTAzgPsmqH-3nPkNZ","type":"POST","ID":"gH2NBTAzgPsmqH-3nPkNZ","cat_id":"cat-1","cat_name":"Category-1","post_content":"awsaw","file_ids":["upw-iXuWf405GHJSlkQvR","jFl7l2bfdpKFaD9pJ97a8","q0aiLRRKT4lGFY6xXKmpj"],"postOwner":"hasib2","tag_persons":["canny","silly","acoustics"],"created_at":"1627783729259","sub":"a63aebde-1325-4bdb-9ff6-a07aa5d2e23e"}',
    'isBase64Encoded': False,
}
