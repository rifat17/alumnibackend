import os
import json
import boto3
from aws_lambda_powertools.utilities.parser import ValidationError
# import requests
from botocore.exceptions import ClientError

# from lambda_layers.python.post.insert.model.post import PostReqBody

from post.model.post import PostReqBody

def lambda_handler(event, context):

    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
    }


    print(event)

    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)

    try:

        bodydict = json.loads(event['body'])
        post = PostReqBody(**bodydict)
        print(post)
    except ValidationError as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }

    try:
        table.put_item(
            Item = post.dict()
        )
    except ClientError as e:
        print(e)
        return {'statusCode': 400, 'body': e.response['Error'], 'headers': headers,}

    else:
        print ('Item inserted')
        return {
            'statusCode': 200,
            'body': post.json(), 
            'headers': headers
        } 



