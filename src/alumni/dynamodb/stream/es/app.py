import os
from elasticsearch import Elasticsearch

es_user = os.environ['ESUSER']
es_password = os.environ['ESUSERPASSWORD']
es_port = 443
es_endpoint = os.environ['ESENDPOINT']
es_endpoint = f'https://{es_endpoint}'
es_client = Elasticsearch([es_endpoint], http_auth=(es_user, es_password), port=es_port)


def add_to_es(index, body, id):
    return es_client.index(index=index, id=id, body=body)


if __name__ == '__main__':
    es_client = Elasticsearch([es_endpoint], http_auth=(es_user, es_password), port=es_port)
    # print(es_client.info())
    print(add_to_es(es_client, index='tespost', id=1, body={'number': [1, 2, 3, 4]}))
