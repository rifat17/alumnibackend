from auth.register.trigger.model import CognitoUserModel
from post.model.post import PostReqBody

# from lambda_layers.python.post.insert.model.post import PostReqBody
# from lambda_layers.python.auth.register.trigger.model import CognitoUserModel


def reWrite(adict):
    for k, v in adict.items():
        if isinstance(v, list):
            items = []
            for item in v:
                items.append(reWrite(item))
            return items
        return v


class POST_Insert():
    @classmethod
    def Model(self):
        return PostReqBody

class POST_Edit():
    @classmethod
    def Model(self):
        return PostReqBody

class POST_Modify():
    @classmethod
    def Model(self):
        return PostReqBody


class USER_Insert():
    @classmethod
    def Model(self):
        return CognitoUserModel


class InvalidOperationError(Exception):
    def __init__(self, value: str, message: str) -> None:
        self.value = value
        self.message = message
        super().__init__(message)


def get_op_model(model_type, op_type):
    allowed_model_types = ['POST', 'USER']
    allowed_op_types = ['INSERT', 'MODIFY',]  # 'MODIFY', 'REMOVE'
    if model_type not in allowed_model_types or op_type not in allowed_op_types:
        raise InvalidOperationError(value=f'{model_type}, {op_type}', message='Not Supported OPERATION')
    reqModel = '_'.join([model_type, op_type])
    models = {
        'USER_INSERT': USER_Insert,
        'POST_INSERT': POST_Insert,
        'POST_MODIFY': POST_Edit,

    }
    return models[reqModel].Model()
