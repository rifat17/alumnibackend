import abc
import json
import os

# import requests

from aws_lambda_powertools.utilities.data_classes.dynamo_db_stream_event import (
    DynamoDBRecordEventName, DynamoDBStreamEvent)

from aws_lambda_powertools.utilities.parser import ValidationError
from botocore.exceptions import ClientError


from aws_lambda_powertools.utilities.parser.models import \
    DynamoDBStreamRecordModel

from utils import reWrite, get_op_model
from es.config import *
from es.app import add_to_es


def lambda_handler(event, context):

    print("STREAM_PROCESS", event)

    
    headers = {
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    
    record: DynamoDBStreamRecordModel = DynamoDBStreamRecordModel(
        **event['Records'][0])

    PK = reWrite(record.dynamodb.Keys.get('PK', ''))
    model_type = PK.split('#')[0]
    

    # TODO:
    # 'REMOVE' event only share OldImage
    # take seperate care for it

    print('record.dynamodb',record.dynamodb)
    data_dict = {}
    # Always store the new/updated value to ES 
    # one day later: wrong idea? , but all event dont include `NewImage`
    # but they do, due to having same key, entity will be updated, not inserted, that is the advantage here
    for k, v in record.dynamodb.NewImage.items():
        data_dict[k] = reWrite(v)

    try:
        Model = get_op_model(model_type, record.eventName)
        instance = Model(**data_dict)
        print(instance)

        # TODO 
        """
        IDEA:
        if model type == post and insrance.post_status(NEW IMAGE) == APPROVE,
        then, trigger SES.
        
        05 sec later, api /post/approve lambda invokation will trigger SES, :P
        """

        add_to_es(index=model_type.lower(), id=instance.PK, body=instance.dict())

    except (ValidationError,ClientError)  as e:
        print(e)
        return {
            'statusCode': 400,
            'body': e.json(),
            'headers': headers,
        }
    return {
        'statusCode': 200, 
        'headers': headers
    } 



def lambda_handler_BK1(event, context):
    event: DynamoDBStreamEvent = DynamoDBStreamEvent(event)
    newData = {}
    for record in event.records:
        PK = record.dynamodb.keys.get('PK', None)
        print(PK.get_value)
        print(PK.get_value.split('#')[0])
        model_type = PK.get_value.split('#')[0]

        if record.event_name == DynamoDBRecordEventName.INSERT:
            r = DynamoDBStreamRecordModel(**event['Records'][0])
            for k, v in r.dynamodb.NewImage.items():
                newData[k] = reWrite(v)
            # print(newData)

            # HOW TO KNOW IT IS POST or USER data?????? think.
            try:

                Model = get_op_model(model_type, record.event_name.name)
                print(Model(**newData))
            except Exception as e:
                print(e)
        else:
            print(event)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            # "location": ip.text.replace("\n", "")
        }),
    }


def lambda_handler2(event, context):
    record: DynamoDBStreamRecordModel = DynamoDBStreamRecordModel(
        **event['Records'][0])

    PK = reWrite(record.dynamodb.Keys.get('PK', ''))
    model_type = PK.split('#')[0]

    data_dict = {}
    for k, v in record.dynamodb.NewImage.items():
        data_dict[k] = reWrite(v)

    # Always store the new/updated value to ES

    if record.eventName == DynamoDBRecordEventName.INSERT.name:
        try:
            Model = get_op_model(model_type, record.eventName)
            instance = Model(**data_dict)
            print(instance)
        except Exception as e:
            print(e)
    elif record.eventName == DynamoDBRecordEventName.MODIFY.name:

        print(event)

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            # "location": ip.text.replace("\n", "")
        }),
    }


demo_post_insert = {
    'Records': [
        {
            'eventID': '946ef40881415799d7ce60b95f69ab59',
            'eventName': 'INSERT',
            'eventVersion': '1.1',
            'eventSource': 'aws:dynamodb',
            'awsRegion': 'us-east-2',
            'dynamodb': {
                'ApproximateCreationDateTime': 1627698871.0,
                'Keys': {
                    'SK': {
                        'S': 'POST#G7Sa_H3cRMzrYXV0CVKgA'
                    },
                    'PK': {
                        'S': 'POST#G7Sa_H3cRMzrYXV0CVKgA'
                    }
                },
                'NewImage': {
                    'postOwner': {
                        'S': 'hasib'
                    },
                    'post_content': {
                        'S': 'Post3'
                    },
                    'cat_name': {
                        'S': 'Category-1'
                    },
                    'tag_persons': {
                        'L': [
                            {
                                'S': 'melted'
                            },
                            {
                                'S': 'perfect'
                            },
                            {
                                'S': 'wiry'
                            },
                            {
                                'S': 'blood'
                            }
                        ]
                    },
                    'SK': {
                        'S': 'POST#G7Sa_H3cRMzrYXV0CVKgA'
                    },
                    'cat_id': {
                        'S': 'cat-1'
                    },
                    'file_ids': {
                        'L': [
                            {
                                'S': 'YqIl-dCis3HGXfZO2ySJy'
                            },
                            {
                                'S': 'zZRF4e2eTAqAStV6IgLgc'
                            },
                            {
                                'S': '9OqTNFdqgXlA3aBa21_jF'
                            }
                        ]
                    },
                    'created_at': {
                        'S': '2021-07-31T08:34:27'
                    },
                    'ID': {
                        'S': 'G7Sa_H3cRMzrYXV0CVKgA'
                    },
                    'PK': {
                        'S': 'POST#G7Sa_H3cRMzrYXV0CVKgA'
                    },
                    'type': {
                        'S': 'POST'
                    },
                    'sub': {
                        'S': '0c807c9c-3954-42df-9394-6c1d64b0ca16'
                    }
                },
                'SequenceNumber': '1549000000000007459667944',
                'SizeBytes': 392,
                'StreamViewType': 'NEW_AND_OLD_IMAGES'
            },
            'eventSourceARN': 'arn:aws:dynamodb:us-east-2:618758721119:table/AlumniDynamoDBTable/stream/2021-07-30T17:57:31.726'
        }
    ]}

demo_post_modify = {'Records': [{
    'eventID': '69d80bffb3ba40edd16106f379f18aea',
    'eventName': 'MODIFY',
    'eventVersion': '1.1',
    'eventSource': 'aws:dynamodb',
    'awsRegion': 'us-east-2',
    'dynamodb': {
        'ApproximateCreationDateTime': 1627750934.0,
        'Keys': {'SK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'},
                'PK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'}},
        'NewImage': {
            'postOwner': {'S': 'hasib'},
            'post_content': {'S': 'PostingPostingPostingPostingPostingPosting'
                            },
            'cat_name': {'S': 'Category-1'},
            'tag_persons': {'L': [{'S': 'pocket'}]},
            'SK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'},
            'cat_id': {'S': 'cat-1'},
            'file_ids': {'L': [{'S': 'w-F5vFz87vlfh2hDhN5qS'},
                            {'S': 'UPTTkc2RDYtBOUxAY7ssq'},
                            {'S': 'iJ-hIb9XrOHlRMDb3Y60R'}]},
            'created_at': {'S': '2021-07-31T08:26:25'},
            'ID': {'S': 'tPHYxnfY6V-oQW1iDjUtO'},
            'PK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'},
            'type': {'S': 'POST'},
            'sub': {'S': '0c807c9c-3954-42df-9394-6c1d64b0ca16'},
        },
        'OldImage': {
            'postOwner': {'S': 'hasib'},
            'post_content': {'S': 'PostingPostingPosting'},
            'cat_name': {'S': 'Category-1'},
            'tag_persons': {'L': [{'S': 'pocket'}]},
            'SK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'},
            'cat_id': {'S': 'cat-1'},
            'file_ids': {'L': [{'S': 'w-F5vFz87vlfh2hDhN5qS'},
                            {'S': 'UPTTkc2RDYtBOUxAY7ssq'},
                            {'S': 'iJ-hIb9XrOHlRMDb3Y60R'}]},
            'created_at': {'S': '2021-07-31T08:26:25'},
            'ID': {'S': 'tPHYxnfY6V-oQW1iDjUtO'},
            'PK': {'S': 'POST#tPHYxnfY6V-oQW1iDjUtO'},
            'type': {'S': 'POST'},
            'sub': {'S': '0c807c9c-3954-42df-9394-6c1d64b0ca16'},
        },
        'SequenceNumber': '3941600000000009240830074',
        'SizeBytes': 743,
        'StreamViewType': 'NEW_AND_OLD_IMAGES',
    },
    'eventSourceARN': 'arn:aws:dynamodb:us-east-2:618758721119:table/AlumniDynamoDBTable/stream/2021-07-30T17:57:31.726'
    ,
}]}


# src/alumni/post/insert/model/post.py
# from src.alumni.post.insert.model.post import AddPostReqBody


def test(data):
    # print(data['Records'])
    #
    event: DynamoDBStreamEvent = DynamoDBStreamEvent(data)
    newData = {}
    for record in event.records:
        PK = record.dynamodb.keys.get('PK', None)
        print(PK.get_value)
        print(PK.get_value.split('#')[0])
        model_type = PK.get_value.split('#')[0]

        if record.event_name == DynamoDBRecordEventName.INSERT:
            r = DynamoDBStreamRecordModel(**data['Records'][0])
            for k, v in r.dynamodb.NewImage.items():
                newData[k] = reWrite(v)
            # print(newData)

            # HOW TO KNOW IT IS POST or USER data?????? think.
            try:

                Model = get_op_model(model_type, record.event_name.name)
                print(Model(**newData))
            except Exception as e:
                print(e)


if __name__ == '__main__':
    test(demo_post_insert)
