import json
import os

from aws_lambda_powertools.utilities.data_classes.cognito_user_pool_event import PostConfirmationTriggerEvent
import boto3
from aws_lambda_powertools.utilities.parser import ValidationError
from botocore.exceptions import ClientError

# import requests
from auth.register.trigger.model import CognitoUserModel

# from aws_lambda_powertools.utilities.parser import BaseModel, Field





def handler(event, context):
    print(event)
    
    table_name = os.environ['TABLE']
    region = os.environ['REGION']
    table = boto3.resource('dynamodb', region_name=region).Table(table_name)

    headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }

    data: PostConfirmationTriggerEvent = PostConfirmationTriggerEvent(event)
    user = None
    print(data)

    if data.trigger_source == 'PostConfirmation_ConfirmSignUp':
        data.request.user_attributes.update({'username': data.user_name})
        user_row_data = data.request.user_attributes

        try:
            user: CognitoUserModel = CognitoUserModel(**user_row_data)
            print(user)
            PK = f'USER#{user.sub}'
            SK = PK
            user.PK = PK
            user.SK = SK
        except ValidationError as e:
            print(e)
            return event

        try:
            table.put_item(
                Item=user.dict()
            )
            ses = boto3.client("ses")
            response = ses.verify_email_identity(
                EmailAddress=user.email
            )
            print(response)
        except ClientError as e:
            print(e)
            return event

        else:
            print('Item inserted')
            return event


def main(event):
    data: PostConfirmationTriggerEvent = PostConfirmationTriggerEvent(event)
    user_data = {}
    print(data.trigger_source)
    # print(data.request.user_attributes)
    # print(data.user_name)
    data.request.user_attributes.update({'username': data.user_name})
    user_data = data.request.user_attributes

    print(CognitoUserModel(**user_data))


if __name__ == '__main__':
    # main(lambda_event)
    lambda_event = {
        'version': '1',
        'region': 'us-east-2',
        'userPoolId': 'us-east-2_AAwlUjNyE',
        'userName': 'hasib2',
        'callerContext': {
            'awsSdkVersion': 'aws-sdk-unknown-unknown',
            'clientId': '3birvdi6c4tqo4db0a5mpn9p37'
        },
        'triggerSource': 'PostConfirmation_ConfirmSignUp',
        'request': {
            'userAttributes':
                {
                    'sub': '124590f7-d845-4568-acd6-21e88eb97910',
                    'cognito:email_alias': 'abdullah.qups@gmail.com',
                    'email_verified': 'true',
                    'cognito:user_status': 'CONFIRMED',
                    'email': 'abdullah.qups@gmail.com'
                }
        },
        'response': {}
    }

    # response = handler(event=lambda_event, context='')
    # print('RESPONSE', response)
    main(lambda_event)
